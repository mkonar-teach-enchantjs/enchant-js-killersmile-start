/** Make a smile bounce around. **/

window.onload = myGame;
enchant();

function myGame() {
    // New game with a 320x320 pixel canavs, 24 frames/sec.
    var game = new Core(320, 320);
    game.fps = 24;

    // Preload assets.
    game.preload('facesmile.png');

    // Specify what should happen when the game loads.
    game.onload = function () {
        // Initial game parameters
        game.rootScene.backgroundColor = 'rgb(222,222,255)';
        
        // Add a smiley:
        var smiley = makeSmiley();
    };
    
    // Start the game.
    game.start();

    // ================================================================
    //    myGame helper functions
    // ================================================================

    /** Make a smiley with the needed listeners.
     *  Return a reference to the created smiley. */
    function makeSmiley() {
        var smiley = new Sprite(32, 32);
        smiley.image = game.assets['facesmile.png'];
        
        // Position smiley in a random place close to the top left.
        smiley.x = randomInt(1, game.width / 4);
        smiley.y = randomInt(1, game.height / 4);
        
        // Add smiley to scene.
        game.rootScene.addChild(smiley);
        
        // Add properties to the smiley sprite to store the speed.
        // Make sure x and y speeds are not the same to avoid boring bouncing.
        smiley.x_speed = 2 * randomInt(1, 3);      // 2, 4 or 6
        smiley.y_speed = 2 * randomInt(1, 3) + 1;  // 3, 5 or 7
        
        // Add properites to smiley to store direction and start the smiley
        // off in a random direction.
        smiley.isMovingRight = true;
        smiley.isMovingDown = true;

        if (Math.random() < 0.5) {
            smiley.isMovingRight = false;
        }

        if (Math.random() < 0.5) {
            smiley.isMovingDown = false;
        }

        // Smiley event listeners.
        smiley.addEventListener(Event.ENTER_FRAME, function () {
            // Bounce off boundaries
            if (smiley.x >= game.width - smiley.width) {
                smiley.isMovingRight = false;
            } else if (smiley.x <= 0) {
                smiley.isMovingRight = true;
            }

            if (smiley.y >= game.height - smiley.height) {
                smiley.isMovingDown = false;
            } else if (smiley.y <= 0) {
                smiley.isMovingDown = true;
            }

            // Do move.
            if (smiley.isMovingRight) {
                smiley.x += smiley.x_speed;
            } else {
                smiley.x -= smiley.x_speed;
            }

            if (smiley.isMovingDown) {
                smiley.y += smiley.y_speed;
            } else {
                smiley.y -= smiley.y_speed;
            }
        });

        return smiley;
    }
    
    /** Generate a random integer between low and high (inclusive). */
    function randomInt(low, high) {
        return low + Math.floor((high + 1 - low) * Math.random());
    }

    // ================================================================
    //    End myGame helper functions
    // ================================================================
}
